works on linux and (probably) macOS

# Use
Copy this script somewhere, make it executable and setup a shortcut on your desktop that leads to the script. This way, you just need to click on the shortcut to start working on a jupyter notebook on your server.

When exiting, click the "Quit" button on the top-right corner to stop the jupyter server. If you want to terminate the SSH tunnel, you can do it with `ps -ef | grep "ssh -NfL localhost" | awk '{print($2)}' | xargs kill`.

## In case you need a token to connect
Since the token might change every time you restart a jupyter server, you can instead set up jupyter to use a fixed password instead, that you can then use to connect without a token.

# Adapt the script to your situation
- Replace `/my/folder` with the path of the folder where you want the jupyter server to run. The access path to some files might depend on this.

- The script opens a SSH tunnel between a local port (on your machine) and a distant port (on your server). In the script I used 8888 as the local port and 8887 as the distant port, but you are free to change that, if some ports are already used.

- You need to input the absolute path to your jupyter executable. You can get this path with the command `which jupyter`.

## If you are on macOS
Replace `xdg-open` with `open`.

## If you are on Windows
You should switch to linux. Or, find a way to execute bash scripts.
