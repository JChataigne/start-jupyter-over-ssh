#!/bin/bash

# setup the ssh tunneling
ssh -NfL localhost:8888:localhost:8887 user@yourserver

# start notebook
ssh -f user@yourserver "cd /my/folder; /absolute/path/to/jupyter notebook --no-browser --port=8887"

# open notebook in browser tab
xdg-open "localhost:8888"
# (xdg-open only works on Unix-based OSes, the above link shows solutions for other OSes)
# see stackoverflow.com/questions/38147620 for more detail


# use the "Quit" button in the UI when exiting, or the server will keep running

# To terminate the tunnel, execute the following on local machine:
# ps -ef | grep "ssh -NfL localhost:8888" | awk '{print($2)}' | xargs kill

# https://github.com/jupyter/notebook/issues/2254
# See the link above in case of token/password issues
